Oceania WordPres Theme
* by Andrei Olaru, http://www.krtv.co

About
Oceania is a clean, flexible WordPress theme started in 2010 as a personal project to have the same theme on all my websites. It was built to make it easier for me to take care of all my website by updating only one theme.

Oceania is:

	1. fast loading
	2. clean looking
	3. very flexible
	4. easy to administrate
	5. responsive and mobile ready
	6. unique in features

These points are always kept in mind when Oceania is developed.