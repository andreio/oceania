<?php
/*
Template Name: Filter Menu
*/
?>
<?php get_header(); ?>

<div id="page">

	<ul id="portfolio">	

	<?php query_posts( 'cat=3&posts_per_page=500');

		if ( have_posts() ) : while ( have_posts() ) : the_post();?>

		<li class="<?php $terms = get_the_terms( $post->id, 'category'); if ($terms) foreach( $terms as $term ) { print $term->slug . ' '; unset($term); }; $tags = wp_get_post_tags($post->ID); if ($tags) foreach($tags as $tag) { print $tag->slug . ' '; unset($tag); }?>">


			<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

			<a href="<?php the_permalink() ?>" class="screenshot" rel="<?php echo $image[0]; ?>" title="<?php the_title(); ?> font">
			<?php the_title(); ?>		
			<img src="<?php echo get_stylesheet_directory_uri() ?>/images/loading.gif" data-original="<?php echo get_stylesheet_directory_uri() ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=100&amp;w=160&amp;zc=1&amp;a=t" alt="<?php the_title(); ?> font" height="100" width="160" class="lazy"/>
			</a>

			<div class="cart">

			<a href="<?php echo home_url(); ?>/go/<?php print basename(get_permalink()); ?>" target="_blank" title="Download <?php the_title(); ?> Font">Download font &darr;</a>

			</div>

		</li>

		<?php endwhile; endif; ?>

	<?php wp_reset_query(); ?>

    </ul>

</div>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/js/screenshotpreview.js"></script>
<?php get_footer(); ?>