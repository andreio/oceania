	<!-- Customize your footer with widgets on three columns -->
	<div id="footer_dynamic">		

		<div class="footer_widget">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Left") ) : ?>

			<ul>

				<li><h3>Footer Left Widget</h3></li>

				<li>Aenean lacinia varius vulputate. Cras in tellus eros, vitae volutpat lectus. Suspendisse nec elit nec mi pretium ullamcorper. Sed a varius massa.</li>

			</ul>		

			<?php endif; ?>

		</div>

		<div class="footer_widget">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Center") ) : ?>

			<ul>

				<li><h3>Footer Center Left Widget</h3></li>

				<li>Aenean lacinia varius vulputate. Cras in tellus eros, vitae volutpat lectus. Suspendisse nec elit nec mi pretium ullamcorper. Sed a varius massa.</li>

			</ul>

			<?php endif; ?>

		</div>

		<div class="footer_widget">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Right") ) : ?>

			<ul>

				<li><h3>Footer Right Widget</h3></li>

				<li>Aenean lacinia varius vulputate. Cras in tellus eros, vitae volutpat lectus. Suspendisse nec elit nec mi pretium ullamcorper. Sed a varius massa.</li>

			</ul>

			<?php endif; ?>

		</div>

	</div>

	<div id="footer_static">

		<!-- Edit the line below with your website info -->
		<a href="<?php echo home_url(); ?>/about">About <?php bloginfo('name'); ?></a> - Since 2011. <a href="<?php echo home_url(); ?>/contact">Contact</a>, <a href="<?php echo home_url(); ?>/terms-of-use-privacy-policy" title="Terms Of Use &amp; Privacy Policy">Terms Of Use &amp; Privacy Policy</a>. Check out <a href="<?php echo home_url(); ?>/popular" title="Popular themes">Popular themes</a>, <a href="<?php bloginfo('rss2_url'); ?>">RSS Feed</a> and <a href="<?php echo home_url(); ?>/sitemap" title="Sitemap">Sitemap</a>
		<br/><br/>

		<!-- Please support WordPress and Oceania by not removing the "Powered by" links. Thank you! -->
		Powered by <a href="http://wordpress.org/">WordPress</a> and <a href="http://www.kreativtheme.com/oceania">Oceania</a>
		<br/><br/>

		<!-- Certified Domain Site Seal by GoDaddy! -->
		<div align="center">
			<a href="http://tracedseals.starfieldtech.com/siteseal/certsealnew?cdSealType=Seal2&amp;sealId=55e4ye7y7mb7365e80b4155fb3180e815ay7mb7355e4ye7ab0e616e4a9807190&referer=http://www.kreativtheme.com" target="_blank" title="Kreativ Theme is certified by GoDaddy.com">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/img/GoDaddy-Certified-Domain.png" alt="Kreativ Theme is certified by GoDaddy.com" width="132" height="31"/>
			</a>
		</div>

		<br/>

		<!-- Do not delete wp_footer -->
		<?php wp_footer(); ?>
		
		
		<!-- Filter JS  -->
		<script type="text/javascript" charset="utf-8">
			$(function() { $("img").lazyload({effect:"fadeIn"}); });
			$(document).ready(function(){ screenshotPreview(); });
		</script>

		<!-- Paste your Google Analitycs code below -->
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-83467-12']);
		  _gaq.push(['_trackPageview']);

		  (function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		</script>

		<!-- AddThis Smart Layers BEGIN -->
		<!-- Go to http://www.addthis.com/get/smart-layers to customize -->
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=andreio"></script>
		<script type="text/javascript">
		  addthis.layers({
			'theme' : 'transparent',
			'share' : {
			  'position' : 'left',
			  'numPreferredServices' : 4
			},  
			'whatsnext' : {}  
		  });
		</script>
		<!-- AddThis Smart Layers END -->

	</div>

</div>

</body>

</html>