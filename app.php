<?php
/*
Template Name: Web App
*/
?>
<!DOCTYPE html>
<html><head>
	<title>Kreativ Font App</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
	<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
	<link rel="stylesheet" href="http://www.kreativfont.com/apps/chrome/style.css" />
	<link rel="shortcut icon" href="http://www.kreativfont.com/apps/chrome/favicon.ico" />
</head>

<body>

<!-- before loading -->
<div id="loading"></div>

<!-- after loading -->
<div id="container">

	<!-- header -->
	<div id="app_header">
		<div id="name"><a href="http://www.kreativfont.com" target="_blank">Font App</a></div>
		<div id="description">Smart Fonts Passion</div>
	</div>

	<!-- content -->
	<div id="app_content">

		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#new" data-toggle="tab">New</a></li>
				<li><a href="#hot" data-toggle="tab">Hot</a></li>
				<li><a href="#deal" data-toggle="tab">Free</a></li>
				<li><a href="#tag" data-toggle="tab">Tag</a></li>
			</ul>

			<div class="tab-content">

				<div class="tab-pane active" id="new">
				
					<div class="small">Here are the latest 10 new fonts</div>
					
					<ul class="app_portfolio">

						<?php 
							$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
							query_posts( 'cat=3&posts_per_page=10');
							if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<li>
								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?> Font" target="_blank">
									<?php the_title(); ?><br/>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<img src="<?php echo get_stylesheet_directory_uri() ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=115&amp;w=200&amp;zc=1&amp;a=t" alt="<?php the_title(); ?> Font" height="115" width="200"/>
								</a>
							</li>
							<?php endwhile; endif; ?>
							
						<?php wp_reset_query(); ?>

					</ul>

				</div>
			
				<div class="tab-pane" id="hot">
				
					<div class="small">Here are the top 10 bestselling fonts</div>

					<ol class="app_portfolio">

						<?php
							query_posts('v_sortby=views&v_orderby=desc&posts_per_page=10');
							if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<li>
								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?> Font" target="_blank">
									<?php the_title(); ?><br/>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<img src="<?php echo get_stylesheet_directory_uri() ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=115&amp;w=200&amp;zc=1&amp;a=t" alt="<?php the_title(); ?> Font" height="115" width="200"/>
								</a>
							</li>
							<?php endwhile; endif; ?>

						<?php wp_reset_query(); ?>

					</ol>
					
				</div>
			
				<div class="tab-pane" id="deal">
				
					<div class="small">Here are the latest 10 free font</div>

					<ul class="app_portfolio">

						<?php 
							$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
							query_posts( 'cat=1519&paged=$page&posts_per_page=10');
							if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<li>
								<a href="<?php the_permalink() ?>" title="<?php the_title(); ?> Font" target="_blank">
									<?php the_title(); ?><br/>
									<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
									<img src="<?php echo get_stylesheet_directory_uri() ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=115&amp;w=200&amp;zc=1&amp;a=t" alt="<?php the_title(); ?> Font" height="115" width="200"/>
								</a>
							</li>
							<?php endwhile; endif; ?>

						<?php wp_reset_query(); ?>

					</ul>

				</div>
				
				<div class="tab-pane justify" id="tag">
				
					<div class="small">Here are the most popular font types</div>

					<?php wp_tag_cloud('number=150'); ?>

				</div>			
					
			</div>
			
		</div>

	</div>	
		
	<!-- footer -->
	<div id="app_footer">
		&copy;2012 by <a href="http://www.kreativfont.com">Kreativ Font</a>	
		
		<!-- load js -->
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>

		<script type="text/javascript">
			jQuery(document).ready(function(){
					$('a[href^="http://"]').attr('target','_blank');
			});
		</script>
		
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-83467-17']);
			_gaq.push(['_trackPageview']);
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>	
	</div>

</div>	
	<script>
		$(window).load(function() {
			//show();
		});
		function show() {
			$('#loading').hide();
			$('#container').fadeIn();
		};

		setTimeout(show, 3000);
	</script>
</body>
</html>