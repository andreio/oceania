<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
	<title><?php global $page, $paged; wp_title(''); ?><?php if(wp_title('', false)) { echo ' - '; } ?><?php bloginfo('name'); if(is_home()) { echo ' - '; bloginfo('description'); } if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'oceania'), max( $paged, $page ) );?></title>
	<!-- Content type -->
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<!-- Google verification-->
	<meta name="google-site-verification" content="LyQN2I8jmKMMYmF2VYjE7iaSw40XEzbuVL8ryAFoipE" />	
	<!-- CSS style -->
	<link type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" />
	<!-- Favicon -->
	<link type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.ico" rel="shortcut icon" />
	<link type="image/x-icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/favicon.ico" rel="icon" />
	<!-- Apple touch icons -->
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri() ?>/img/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri() ?>/img/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri() ?>/img/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri() ?>/img/apple-touch-icon.png" />
	<!-- Pingback url -->
	<link type="image/php" href="<?php bloginfo('pingback_url'); ?>" rel="pingback" />
	<!-- Wordpress head-->
	<?php wp_head(); ?>
</head>
<!-- Body content-->
<body <?php echo body_class(); ?>>
<div id="container">

	<div id="header">

		<div id="logo">	
				
			<div class="switcherMenu">
				<ul class="dropdown">
					<li><a href="<?php echo home_url(); ?>" title="Kreativ Theme - Smart themes passion" class="active"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/kreativtheme.png" alt="Kreativ Theme" width="40" height="40"/>Kreativ Theme<img src="<?php echo get_stylesheet_directory_uri() ?>/img/arrow_down.png" alt="Arrow"></a></li>
					
					<li class="second"><a href="http://www.kreativfont.com" title="Kreativ Font - Smart fonts passion"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/kreativfont.png" alt="Kreativ Font" width="32" height="32">Kreativ Font</a></li>
					
					<li class="second"><a href="http://www.kreativsounds.com" title="Kreativ Sound - Smart sounds passion"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/kreativsound.png" alt="Kreativ Sound" width="32" height="32">Kreativ Sound</a></li>					
				</ul>
			</div>

		</div>

		<div id="search">

			<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<input id="searchi" type="text" placeholder="Search themes, plugins and tips" name="s">
				<button id="searchb" type="submit" ><img src="<?php echo get_stylesheet_directory_uri() ?>/img/search_lens.png"></button>
			</form>
			
		</div>
		
		<div id="controls">

			<?php wp_nav_menu(); ?>
			
		</div>
		
		<div id="signin">
		
			<div class="accountMenu">
			<?php			

				if ( is_user_logged_in() ) {

					echo '	<ul class="dropdown">

							<li><a href="http://www.kreativtheme.com/member/details/" title="Account details" class="active" >My account</a></li>
								
							<li><a href="http://www.kreativtheme.com/member/change-password/" title="Change password">Password</a></li>
								
							<li><a href="'; echo wp_logout_url( home_url() ); echo '" title="Logout">Logout</a></li>
								
						</ul>
						';

				} else {

					echo '	<ul class="dropdown">

								<li><a href="http://www.kreativtheme.com/member/login/" title="Account login" class="active">Login</a></li>

								<li><a href="http://www.kreativtheme.com/member/join/" title="Register account">Register</a></li>
								
							</ul>';

				}			
			?>

			</div>

		</div>

	</div>

	<div id="pseudo">

	</div>

	<?php

		if ( !is_user_logged_in() ) {

			echo	'<div id="join">

						<h2><a href="http://www.kreativtheme.com/go/register" title="Join Today to get ALL our Theme Freebies">Join Today to get ALL our Theme Freebies</a></h2>

						Get 100% complete access to the entire Kreativ Theme collection of <em>free</em> themes!

					</div>';
		}

	?>