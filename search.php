<?php get_header(); ?>

<div id="blog">

<div id="post">

	<?php if (have_posts()) : ?>
		<a name="content"></a>
		<h1>Search Results</h1>
		<div id="post-nav">
			<div class="post-nav-previous"><?php next_posts_link('&larr; Older articles') ?></div>
			<div class="post-nav-next"><?php previous_posts_link('Newer articles &rarr;') ?></div>
		</div>

		<?php while (have_posts()) : the_post(); ?>

			<div class="post">
				<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<small><?php the_time('l, F jS, Y') ?></small>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div id="post-nav">
			<div class="post-nav-previous"><?php next_posts_link('&larr; Older articles') ?></div>
			<div class="post-nav-next"><?php previous_posts_link('Newer articles &rarr;') ?></div>
		</div>

		<?php else : ?>
			<h2>No posts found. Try a different search?</h2>
		<?php endif; ?>
	</div>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>