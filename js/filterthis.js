/*
 * Filter This 0.2.4 jQuery Plugin
 * 
 * Copyright 2012, Andrei Olaru. All rights reserved.
 *
 * You need to purchase a license if you want to use this script on your website.
 * 
 */
 
$(window).ready(function() {

	
	/* Vars */
	
	var max = $("ul#portfolio li").size();
	
	$("#filter-items").text(max);
	
	$("#filter-max").text(max);
	
	
	/* Tag filter */
	
	$('.greybox a').click(function() {
	
		var count = 0;
	
		$(this).css('outline','none');
		
		$('.greybox .current').removeClass('current');
		
		$(this).parent().addClass('current');
		
		var filterVal = $(this).text().toLowerCase().replace(' ','-');
				
		if(filterVal == 'reset') {
			
			$('ul#portfolio li.hidden').fadeIn('slow').removeClass('hidden');
			
			$("#filter-items").text(max);
		
		}
	
		else {
			
			$('ul#portfolio li').each(function() {
			
				if(!$(this).hasClass(filterVal)) {
					
					$(this).fadeOut('slow').addClass('hidden');

				} 
				
				else {
					
					$(this).fadeIn('slow').removeClass('hidden');
					
					count++;

				}
			});
			
			$("#filter-items").text(count);
			
		}
		
		return false;
		
	});
	
	/* Filter menu bar is pinned on top  */
	
	$(window).scroll(function(e) {
	
		if ($(window).scrollTop() > 220) { 
			
			$('#filter').css({
				
				position: 'fixed',
				
				top: '0'
			
			});
			
		} else {
			
			$('#filter').css({
			
				position: 'relative'
				
			});
			
		}
		
	});
	
});