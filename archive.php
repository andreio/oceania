<?php get_header(); ?>

<div id="blog">

	<div id="post">
	
		<div class="post_category">
			
			<?php /* If this is a category archive */ if (is_category()) { ?>
		
			<h1><?php single_cat_title(); ?> themes</h1>
			
			<?php echo category_description(); ?>
			
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
			
			<h1 class="archive">Tagged as <?php single_tag_title(); ?></h1>
			
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			
			<h1 class="archive"><?php the_time('F jS, Y'); ?> archive</h1>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			
			<h1 class="archive"><?php the_time('F, Y'); ?> archive</h1>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			
			<h1 class="archive"><?php the_time('Y'); ?> archive</h1>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			
			<h1 class="archive">Archives</h1>

			<?php } ?>
		
		</div>
		
		<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 
		
			$post = $wp_query->post;
			
			?>
		
			<div class="post_archive"> 
			
				<?php
				
				if ( has_post_thumbnail() ) {
				
				?>

					<div class="post_thumbnail">
			
						<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">

						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>

						<img src="<?php echo get_stylesheet_directory_uri() ?>/img/loading.gif" data-original="<?php echo get_stylesheet_directory_uri() ?>/timthumb.php?src=<?php echo $image[0]; ?>&amp;h=100&amp;w=160&amp;zc=1&amp;a=t" alt="<?php the_title(); ?> theme" height="100" width="160" class="lazy"/>
			
						</a>
					
					</div>

				<?php } ?>

					<h2 class="archive"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					
					<div class="post_meta">Published <!-- by <?php the_author() ?> --> on <?php the_time('l, F jS, Y') ?> in <?php the_category(', ') ?>.</div>
					
					<div class="post_content"><?php the_content('<br/>Read the rest of this article &raquo;'); ?></div>
			
			</div>
			
		<?php
		
		endwhile; else: 
		
		?>
	
		<div class="post_archive">
		
			<h2>Article Not Found</h2>
			
			Ooops! We are sorry, but the article you are looking for does not exist or has been moved.<br/>
			
			Please use the search form to find what you are looking for.<br/>
			
			Thank you! <br/>
			
		</div>
		
		<?php endif; ?>
	
		<div id="post_nav">
		
			<?php if (function_exists( 'wp_pagenavi' )) : wp_pagenavi();
				  
				  else : ?>
					
					<div class="post_nav_previous"><?php next_posts_link(' &larr; Older articles ') ?></div>
		
					<div class="post_nav_next"><?php previous_posts_link(' Newer articles &rarr; ') ?></div>
					
			<?php endif; ?>
			
		</div>		
	
	</div>

	<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>