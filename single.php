<?php get_header(); ?>

<div id="blog">

	<div id="post" <?php post_class(); ?>>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<h1><?php the_title(); ?></h1>

		<div class="post_meta"> 
			Published <!-- by <?php the_author() ?> --> on <?php the_time('l, F jS, Y') ?> in <?php the_category(', ') ?>.
			<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) { 
			// Both Comments and Pings are open ?>
			<?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?>
			<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) { 
			// Only Pings are Open ?>
			Comments are closed, but you can <a href="<?php trackback_url(); ?> ">trackback</a> from your own site.
			<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) { 
			// Comments are open, Pings are not ?>
			<?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?>, pings are closed.
			<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
			// Neither Comments, nor Pings are open ?>
			Comments and pings are closed.
			<?php } ?>
			<?php if(function_exists('the_views')) { the_views(); } ?>			
			<?php edit_post_link('Edit','',''); ?>			
		</div>

		<div class="post_content">
			<?php the_content('Read the rest of this article &larr;'); ?>
			<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			<?php if(function_exists('the_ratings')) { echo "<h3>"; wp_title("",true); echo " theme ratings</h3>"; the_ratings(); echo "<br/>";} ?>
			<?php wp_related_posts()?>
		</div>
		
		<div class="post_author">		
			<?php if (function_exists('get_avatar')) { echo "<div class=\"post_author_avatar\">" . get_avatar( get_the_author_meta('email'), '64' ) . "</div>"; }?>
			<div class="post_author_text">
				<h3>About <?php the_author_posts_link(); ?></h2>
				<?php the_author_meta('description'); ?>
			</div>
		</div>
		
		<div class="post_tags">
			<?php the_tags( 'Tags:  ', ', ', ''); ?>
		</div>
		
		<div class="post_nav_previous">
			<?php previous_post_link( '%link', _x( '&larr;', 'Previous article', 'oceania' ) . ' %title' ); ?>
		</div>
		
		<div class="post_nav_next">
			<?php next_post_link( '%link', '%title' . _x( '&rarr;', 'Next article', 'oceania' )); ?>
		</div>
	
		<?php comments_template(); ?>

		<?php endwhile; else: ?>
		Sorry, no articles matched your criteria.

		<?php endif; ?>

	</div>

<?php get_sidebar(); ?>

</div>

<?php get_footer(); ?>